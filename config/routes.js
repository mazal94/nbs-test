/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

   /*************************************************************
   * Server Rendered HTML Page Endpoints                        *
   *************************************************************/

  '/': { view: 'pages/homepage' },
  

  /*************************************************************
   * JSON API ENDPOINTS                                         *
   *************************************************************/
  
  'GET /distance' : 'CitiesSearchController.distance',
  'GET /popularSearch' : 'CitiesSearchController.popularSearch',
  'GET /popularFiveSearch' : 'CitiesSearchController.popularFiveSearch'


};
