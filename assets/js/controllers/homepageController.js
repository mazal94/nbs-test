angular.module('nbsTest').controller('homepageController', ['$scope', '$http', 'toastr', function($scope, $http, toastr) {
    //flag for the spinner (on btn-calc)
    $scope.loading = false;

    //the function calculate the distance between two cities
    $scope.calcDis = function() {
        $scope.loading = true; //turn on the spinner

        //check if source city or destination city are not null
        if (!$scope.srcAddr || $scope.srcAddr == '') { //invalid source city
            toastr.error('הכנס את עיר מוצא', {
                closeButton: true
            });
            $scope.loading = false;
            return;
        }
        if (!$scope.destAddr || $scope.destAddr == '') { //invalid destination city
            toastr.error('הכנס את כתובת יעד', {
                closeButton: true
            });
            $scope.loading = false;
            return;
        }

        //input validity of source address        
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': $scope.srcAddr }, function(results, status) {
            if (status == 'OK') {

                //input validity of destination address
                geocoder.geocode({ 'address': $scope.destAddr }, function(results, status) {
                    if (status == 'OK') {

                        //get the distance from the server
                        $http.get('/distance?sAddr=' + $scope.srcAddr + '&dAddr=' + $scope.destAddr)
                            .then(function onSuccess(sailsResponse) {
                                $scope.distance = sailsResponse.data.distance;

                                //the function send request to the server to get the most popular searche
                                $http.get('/popularSearch')
                                    .then(function onSuccess(sailsResponse) {
                                        $scope.popularDis = sailsResponse.data.distance;
                                        $scope.pSrcAddr = sailsResponse.data.srcAddr;
                                        $scope.pDestAddr = sailsResponse.data.destAddr;

                                        //the function send request to the server to get the most popular searches (5 results)
                                        $http.get('/popularFiveSearch')
                                            .then(function onSuccess(sailsResponse) {
                                                $scope.popularFiveLst = sailsResponse.data.popularFive;

                                                $scope.srcAddr = "";
                                                $scope.destAddr = "";
                                                $scope.loading = false; //turn off the spinner
                                                $("#fsModal").modal();
                                            })
                                            .catch(function onError(sailsResponse) {
                                                toastr.error('ארעה שגיאה בלתי צפויה', 'שגיאה', {
                                                    closeButton: true
                                                });
                                                $scope.loading = false;
                                                return;
                                            });
                                    })
                                    .catch(function onError(sailsResponse) {
                                        toastr.error('ארעה שגיאה בלתי צפויה', 'שגיאה', {
                                            closeButton: true
                                        });
                                        $scope.loading = false;
                                        return;

                                    });
                            })
                            .catch(function onError(sailsResponse) {
                                toastr.error('אחת מהכתובות שהזנת אינן תקינות', {
                                    closeButton: true
                                });
                                $scope.loading = false;
                                return;
                            });
                    }
                    else {
                        toastr.error('עיר יעד אינה תקינה', {
                            closeButton: true
                        });
                        $scope.loading = false;
                        return;
                    }
                });
            }
            else {
                toastr.error('עיר מוצא אינה תקינה', {
                    closeButton: true
                });
                $scope.loading = false;
                return;
            }
        });
    };
}]);
