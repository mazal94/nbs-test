/**
 * CitiesSearchController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var distance = require("google-distance");
distance.apiKey = 'AIzaSyBS7zCXubIAqt9fCAwy1VQyZWnMWxOtROE';

module.exports = {
    distance: function(req, res) {
        //searching for the distance on database
        CitiesSearch.findOne({
                or: [{ sourceAddr: req.param('sAddr'), destAddr: req.param('dAddr') },
                    { destAddr: req.param('sAddr'), sourceAddr: req.param('dAddr') }
                ],
            })
            .exec(function(err, result) {
                if (err) {
                    return res.badRequest(err);
                }
                else if (!result) { //if the distance is not alredy saved on db
                    //calculate distance
                    distance.get({
                            origin: req.param('sAddr'),
                            destination: req.param('dAddr')
                        },
                        function(err, data) {
                            if (err) return res.badRequest(err);

                            var options = {
                                sourceAddr: req.param('sAddr'),
                                destAddr: req.param('dAddr'),
                                distance: data.distance.split(" ")[0],
                                searchCounter: 1
                            };

                            //create new distance search on db
                            CitiesSearch.create(options).
                            exec(function(err, createdCitiesSearch) {
                                if (err) {
                                    return res.badRequest(err);
                                }

                                return res.ok({
                                    distance: options.distance
                                });
                            });

                        });
                }
                else { //the distance is alredy saved on db -> update the counter
                    CitiesSearch.updateOne({ id: result.id })
                        .set({
                            searchCounter: result.searchCounter + 1
                        }).exec(function(err, newResult) {
                            if (newResult) {
                                return res.ok({
                                    distance: newResult.distance
                                });
                            }
                            else {
                                sails.log('The database does not contain the specific search');
                                return res.badRequest(err);
                            }
                        });
                }
            });

    },

    //search for the most popular search on db
    popularSearch: function(req, res) {
        CitiesSearch.getDatastore().sendNativeQuery(
            'select * from citiessearch as t inner join (select max(searchCounter) as popular from citiessearch) as r on r.popular=t.searchCounter;', [],
            function(err, result) {
                if (err || !result || result.length == 0) { //if no search alredy saved on db
                    return res.ok('No search alredy saved on db');
                }
                else {
                    return res.ok({
                        srcAddr: result.rows[0].sourceAddr,
                        destAddr: result.rows[0].destAddr,
                        distance: result.rows[0].distance
                    });
                };
            });
    },

    //search for the top five most popular search on db
    popularFiveSearch: function(req, res) {
        CitiesSearch.find()
            .sort('searchCounter DESC') //sort by descending order
            .limit(5) //get only the first 5 results
            .exec(function(err, newResult) {
                if (err) return res.badRequest(err);
                else return res.ok({
                    popularFive: newResult
                });
            });
    }

};
