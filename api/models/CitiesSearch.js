/**
 * CitiesSearch.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    sourceAddr:{
      type: 'string'
    },
    
    destAddr:{
      type: 'string'
    },
    
    distance: {
      type: 'string'
    },
    
    searchCounter:{
      type: 'number'
    }
    
  },

};

